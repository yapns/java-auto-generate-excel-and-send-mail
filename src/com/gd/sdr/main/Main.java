/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd.sdr.main;

import com.gd.sdr.dao.UserDAO;
import org.apache.log4j.Logger;


/**
 *
 * @author naisiong.yap
 */
public class Main {
     private static Logger logger = Logger.getLogger(Main.class);
    public static void main(String[] args) throws Exception {
        logger.info("======================================");
        logger.info("==========Start to Generate==========");
        logger.info("======================================");
        
           int totalDays = 30;     //Define total days that want to generate, count from start date
         String reportStartDate = "2014-02-03";  //Define a start date
       //   String reportStartDate = null;  //Never define a start date

        if (reportStartDate != null){
          UserDAO.generateSheet1_Summary(reportStartDate, totalDays);  
        }
        else {
        String startdate= UserDAO.getDate("ASC");
        String enddate =  UserDAO.getDate("DESC");

        UserDAO.getAllDate(startdate,enddate);
       }

        logger.info("======================================");
        logger.info("==========The End==========");
        logger.info("====================================== \n");
    }
}
