/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd.sdr.mail;


import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

/**
 *
 * @author naisiong.yap
 */
public class SendAttachMail {
    private static Logger logger = Logger.getLogger(SendAttachMail.class);
    public static String SendMail (String strfile){
         logger.info("<<start send mail>>");
        try{      
            String host = PropertyUtil.getProperty("HOST");
            String from = PropertyUtil.getProperty("FROM");
            String pwd = PropertyUtil.getProperty("PWD");
            String port = PropertyUtil.getProperty("PORT");
            String to = PropertyUtil.getProperty("TO");
            
            String subject = PropertyUtil.getProperty("SUBJECT");
            String msg = PropertyUtil.getProperty("MESSAGE");

            Properties props = System.getProperties();  
            props.put("mail.smtp.starttls.enable","true");  
            props.put("mail.smtp.host", host);  
            props.put("mail.smtp.user", from);  
            props.put("mail.smtp.password", pwd);  
            props.put("mail.smtp.port", port);   
            props.put("mail.smtp.auth", "true");  
            
            Session ses = Session.getDefaultInstance(props, null);  
            MimeMessage message = new MimeMessage(ses);  
            message.setFrom(new InternetAddress(from));  
 
            MimeBodyPart messageBodyPart =  new MimeBodyPart();  
            messageBodyPart.setText(msg);  
            
            Multipart multipart = new MimeMultipart();  
            multipart.addBodyPart(messageBodyPart);  

            messageBodyPart = new MimeBodyPart();  
            DataSource source =  new FileDataSource(strfile);  
            messageBodyPart.setDataHandler(new DataHandler(source));  
            messageBodyPart.setFileName(strfile);  
            multipart.addBodyPart(messageBodyPart);  
            
            message.setContent(multipart);  
            String[] toadd = null;

             if (to.length() != 0) {
                toadd = to.split(",");
                InternetAddress[] toAddress = new InternetAddress[toadd.length];
                for (int i = 0; i < toadd.length; i++) {
                    toAddress[i] = new InternetAddress(toadd[i]);
                }
                message.addRecipients(Message.RecipientType.TO, toAddress);
                message.setRecipients(Message.RecipientType.TO, toAddress);
            }
        
            message.setSubject(subject); 
              
            Transport transport = ses.getTransport("smtp");  
            transport.connect(host, from, pwd);  
            transport.sendMessage(message, message.getAllRecipients());  
            System.out.println("mail sent");  
            transport.close();  
        logger.info("<<end send mail>>");      
        }  
        catch (Exception e){  
            System.out.println(e.toString());  
        }
        return null;
   }
}
