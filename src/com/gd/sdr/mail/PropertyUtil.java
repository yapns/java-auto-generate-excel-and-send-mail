/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd.sdr.mail;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author naisiong.yap
 */
public class PropertyUtil {
    private static Logger logger = Logger.getLogger(PropertyUtil.class);

    public static String getProperty(String fieldName) {
        return getProperty("Mail",fieldName);
    }

    public static String getProperty(String prop,String fieldName) {
        try {
            ResourceBundle.clearCache();
            ResourceBundle resourceBundle = ResourceBundle.getBundle(prop);
            return resourceBundle.getString(fieldName);
        } catch (Exception e) {
            logger.info("getProperties() - Exception["+e+"]");
            return null;
        }
    }
}
