/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd.sdr.object;


/**
 *
 * @author naisiong.yap
 */
public class Report {
    private int count;
    private String createDate;
    private String name;
    private String description;
    
    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
    
    public void setDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDate() {
        return createDate;
    }    
    
   public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public void setEmail(String description) {
        this.description = description;
    }

    public String getEmail() {
        return description;
    }
}
